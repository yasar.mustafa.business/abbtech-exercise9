package org.example.pattern1.component;

public class IntelGraphics implements Graphics {

    @Override
    public void displayGPU() {
        System.out.println("This is Intel GPU");
    }
}
