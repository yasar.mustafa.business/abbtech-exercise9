package org.example.pattern1.component;

public interface Processor {
    void displayProcessor();
}
