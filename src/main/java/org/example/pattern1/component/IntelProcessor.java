package org.example.pattern1.component;

public class IntelProcessor implements Processor{
    @Override
    public void displayProcessor() {
        System.out.println("This is Intel processor");
    }
}
