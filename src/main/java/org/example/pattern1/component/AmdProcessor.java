package org.example.pattern1.component;

public class AmdProcessor implements Processor{
    @Override
    public void displayProcessor() {
        System.out.println("This is AMD processor");
    }
}
