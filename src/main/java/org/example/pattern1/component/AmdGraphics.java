package org.example.pattern1.component;

public class AmdGraphics implements Graphics {
    @Override
    public void displayGPU() {
        System.out.println("This is AMD GPU");
    }
}
