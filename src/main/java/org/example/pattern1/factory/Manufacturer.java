package org.example.pattern1.factory;

import org.example.pattern1.component.Graphics;
import org.example.pattern1.component.Processor;

public abstract class Manufacturer {

    public abstract Processor getProcessor();

    public abstract Graphics getGraphics();

}
