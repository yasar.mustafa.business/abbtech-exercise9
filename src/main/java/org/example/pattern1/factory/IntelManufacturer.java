package org.example.pattern1.factory;

import org.example.pattern1.component.Graphics;
import org.example.pattern1.component.IntelGraphics;
import org.example.pattern1.component.IntelProcessor;
import org.example.pattern1.component.Processor;

public class IntelManufacturer extends Manufacturer {
    @Override
    public Processor getProcessor() {
        return new IntelProcessor();
    }

    @Override
    public Graphics getGraphics() {
        return new IntelGraphics();
    }
}
