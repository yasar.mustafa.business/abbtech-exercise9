package org.example.pattern1.factory;

import org.example.pattern1.component.AmdGraphics;
import org.example.pattern1.component.AmdProcessor;
import org.example.pattern1.component.Graphics;
import org.example.pattern1.component.Processor;

public class AmdManufacturer extends Manufacturer {
    @Override
    public Processor getProcessor() {
        return new AmdProcessor();
    }

    @Override
    public Graphics getGraphics() {
        return new AmdGraphics();
    }
}
