package org.example.pattern1.factory;

public class FactoryProducer {
    public static Manufacturer getManufacturer(Company name){
        if(name == Company.AMD){
            return new AmdManufacturer();
        }
        else if(name == Company.INTEL){
            return new IntelManufacturer();
        }
        return null;
    }

}
