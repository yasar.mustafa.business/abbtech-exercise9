package org.example.pattern1;

import org.example.pattern1.component.Graphics;
import org.example.pattern1.component.Processor;
import org.example.pattern1.factory.Company;
import org.example.pattern1.factory.FactoryProducer;
import org.example.pattern1.factory.Manufacturer;

public class Main {
    public static void main(String[] args) {

        Manufacturer manufacturer1 = FactoryProducer.getManufacturer(Company.AMD);
        if(manufacturer1 != null) {
            Processor processor = manufacturer1.getProcessor();
            Graphics graphics = manufacturer1.getGraphics();

            processor.displayProcessor();
            graphics.displayGPU();
        }
        else{
            System.out.println("Not such a manufacturer");
        }
        System.out.println();
        Manufacturer manufacturer2 = FactoryProducer.getManufacturer(Company.INTEL);
        if(manufacturer2 != null){
            Processor processor = manufacturer2.getProcessor();
            Graphics graphics = manufacturer2.getGraphics();

            processor.displayProcessor();
            graphics.displayGPU();
        }
        else{
            System.out.println("Not such a manufacturer");
        }

    }
}