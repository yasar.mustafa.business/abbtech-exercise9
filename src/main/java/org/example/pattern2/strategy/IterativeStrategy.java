package org.example.pattern2.strategy;

public class IterativeStrategy implements SolvingStrategy{
    @Override
    public int calculateFactorial(int n) {
        if (n < 0) {
            throw new RuntimeException("Please give positive number");
        }

        int result = 1;
        for (int i = 1; i <= n; i++) {
            result *= i;
        }
        return result;
    }
}
