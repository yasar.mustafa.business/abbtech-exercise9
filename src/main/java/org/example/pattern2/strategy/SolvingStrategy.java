package org.example.pattern2.strategy;

public interface SolvingStrategy {

    public int calculateFactorial(int n);

}
