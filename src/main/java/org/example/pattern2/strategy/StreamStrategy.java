package org.example.pattern2.strategy;

import java.util.stream.IntStream;

public class StreamStrategy implements SolvingStrategy{
    @Override
    public int calculateFactorial(int n) {
        if (n < 0) {
            throw new RuntimeException("Please give positive number");
        }
        return IntStream.rangeClosed(1, n)
                .reduce(1, (x, y) -> x * y);
    }
}
