package org.example.pattern2.strategy;

public class RecursiveStrategy implements SolvingStrategy{
    @Override
    public int calculateFactorial(int n) {
        if (n < 0) {
            throw new RuntimeException("Please give positive number");
        }

        if (n == 0 || n == 1) {
            return 1;
        } else {
            return n * calculateFactorial(n - 1);
        }
    }
}
