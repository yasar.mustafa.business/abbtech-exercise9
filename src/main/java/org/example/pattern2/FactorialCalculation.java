package org.example.pattern2;

import org.example.pattern2.strategy.SolvingStrategy;

public class FactorialCalculation {
    private SolvingStrategy solvingStrategy;

    public FactorialCalculation(SolvingStrategy solvingStrategy) {
        this.solvingStrategy = solvingStrategy;
    }

    public int applyStrategy(int n){
        return solvingStrategy.calculateFactorial(n);
    }
}
