package org.example.pattern2;

import org.example.pattern2.strategy.IterativeStrategy;
import org.example.pattern2.strategy.RecursiveStrategy;
import org.example.pattern2.strategy.SolvingStrategy;
import org.example.pattern2.strategy.StreamStrategy;

public class Main {
    public static void main(String[] args) {

        int n = 8;
        try {
            SolvingStrategy iterativeStrategy = new IterativeStrategy();
            FactorialCalculation fc1 = new FactorialCalculation(iterativeStrategy);
            System.out.println("Iterative Approach: " + fc1.applyStrategy(n));

        }catch (Exception ex){
            System.err.println("Negative input For Iterative Approach!!!");
        }

        try {
            SolvingStrategy recursiveStrategy = new RecursiveStrategy();
            FactorialCalculation fc2 = new FactorialCalculation(recursiveStrategy);
            System.out.println("Recursive Approach: " + fc2.applyStrategy(n));
        }catch (Exception ex){
            System.err.println("Negative input for Recursive Approach!!!");
        }

        try {
            SolvingStrategy streamStrategy = new StreamStrategy();
            FactorialCalculation fc3 = new FactorialCalculation(streamStrategy);
            System.out.println("Streams Approach: " + fc3.applyStrategy(n));
        }catch (Exception ex){
            System.err.println("Negative input for Streams Approach!!!");
        }

    }
}